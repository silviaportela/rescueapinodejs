var express = require("express"),
  bodyParser = require("body-parser"),
  app = express(),
  http = require("http"),
  mongoose = require('mongoose');

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

mongoose.connect("mongodb://localhost:27017/API", { useNewUrlParser: true })
const db = mongoose.connection
db.on('error', (error) => console.error(error))
db.once('open', () => console.log('conectado a mongo'))

const routerUser = require('./routes/userR');
const routerAlert = require('./routes/alertR');
const routerTrack = require('./routes/trackR');
const routerMessage = require('./routes/messageR');
const routerZona = require('./routes/zonaR');
var cors = require ('cors')

app.use(cors())
app.use('/user', routerUser);
app.use('/alerts', routerAlert);
app.use('/tracks', routerTrack);
app.use('/messages', routerMessage);
app.use('/zonas', routerZona);

app.listen(3000, () => console.log('Escuchando peticiones en 3000'))




//Socket.io SERVER
var app = require('express')();
var http = require('http').createServer(app);
var io = require('socket.io')(http);

io.on('connection', function (socket) {
  console.log('Un cliente se ha conectado', socket.id);
  
  socket.on('mensaje From client', function (data) {
    sendBroadcastMessage(data)
  });
  
  socket.on('notification', function (data) {
    sendNotification(data)
  });
  socket.on('disconnect', function (socket) {
    console.log('Un cliente se ha desconectado', socket);
  });
});

function sendBroadcastMessage(mensaje){
  io.emit('broadcast', mensaje);
}

function sendNotification(mensaje){
  console.log('A enviar notificacion', mensaje);
  io.emit('notification', mensaje);
}


http.listen(2222, function () {
  console.log('Escuchando CHAT en :2222');
});

