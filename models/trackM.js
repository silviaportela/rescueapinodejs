var mongoose = require('mongoose'),
  Schema = mongoose.Schema;

var trackM = new Schema({
  idTrack: { type: String },
  idAlert: { type: String },
  coordinates: { type: Array },
  user: {type:String}
});

module.exports = mongoose.model('tracks', trackM);
