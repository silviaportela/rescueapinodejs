var mongoose = require('mongoose'),
    Schema   = mongoose.Schema;

var users = new Schema({
  name:{ type: String },
  apellido:{type:String}
});

module.exports = mongoose.model('users', users);
