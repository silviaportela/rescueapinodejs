var mongoose = require('mongoose'),
  Schema = mongoose.Schema;

var zonaM = new Schema({
  idZona: { type: String },
  idAlert: { type: String },
  coordinates: { type: Array },
});

module.exports = mongoose.model('zonas', zonaM);
