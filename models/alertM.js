var mongoose = require('mongoose'),
    Schema   = mongoose.Schema;
    var ObjectId = require('mongodb').ObjectID;

var alertasM = new Schema({
  id:{ type: String },
  name:{ type: String },
  coord1:{ type: Number },
  coord2:{ type: Number },
  status:{type:Number},
  city: { type: String },
  date: { type: Number},
  users:{type:Array},
  puntosImportantes:{type:Array}
});

module.exports = mongoose.model('alerts', alertasM);
