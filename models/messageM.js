var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

messageM = new Schema({
    idMessage: { type: String },
    idAlert: { type: String },
    user:{type:String},
    timestamp: { type: String },
    message: { type: String},
});

module.exports = mongoose.model('messages', messageM);
