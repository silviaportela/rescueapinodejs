const express = require('express')
var app = express();
var ZonaController = require('../controllers/zonaC');

app.get('/:id', ZonaController.getZonasByAlertId)
app.post('/', ZonaController.insertZona)
app.delete('/:id', ZonaController.deleteZona)
 
 module.exports = app;