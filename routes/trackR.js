const express = require('express')
var app = express();
var TrackController = require('../controllers/trackC');

app.get('/', TrackController.getTracks)
app.get('/:id', TrackController.getTracksByAlertId)
app.get('/user/:user', TrackController.getTracksByUser)
app.post('/', TrackController.insertTrack)
app.delete('/:id', TrackController.deleteTrack)

 
 module.exports = app;