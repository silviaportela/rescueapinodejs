const express = require('express')
var app = express();
var MessageController = require('../controllers/messageC');

app.get('/', MessageController.getMessages)
app.get('/:id', MessageController.getMessagesByAlertId)
app.post('/', MessageController.insertMessage)
app.delete('/:idMessage', MessageController.deleteOneMessage)
app.delete('/all/:idAlert', MessageController.deleteAllMessage)
module.exports = app;