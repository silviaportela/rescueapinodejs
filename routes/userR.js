const express = require('express')
var app = express();
var UserController = require('../controllers/userC');

app.get('/:name', UserController.getUserById)
app.post('/createUser', UserController.createUser)


module.exports = app;