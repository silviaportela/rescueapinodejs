const express = require('express')
var app = express();
var AlertController = require('../controllers/alertC');

app.get('/', AlertController.getAlerts)
app.get('/:id', AlertController.getAlertsById)
app.get('/count/:id', AlertController.getAlertsByIdAndCount)
app.get('/api/ordered', AlertController.getAlertsOrdered)
app.post('/', AlertController.insertAlert)
app.delete('/:id', AlertController.deleteAlert)
app.put('/putAlert/:name', AlertController.updateAlert)
app.get('/userAlerts/:name',AlertController.getMyAlerts)
app.get('/userNoAlerts/:name',AlertController.getNoMyAlerts)
app.put('/jointo/:name',AlertController.joinToAlert)

 module.exports = app;