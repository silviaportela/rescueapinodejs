var zonaModel = require('../models/zonaM');
var ObjectId = require('mongodb').ObjectID;



function getZonasByAlertId(req, res, next) {
    var nameForSearch = req.params.id
    zonaModel.find({ idAlert: nameForSearch }, (err, json) => {

        //Si la bbdd no responde
        if (err) {
            return res.status(500).json({
                ok: false,
                mensaje: 'Error al buscar tracks',
                errors: err
            });
        }

        //si el usuario no existe
        if (!json) {
            return res.status(400).json({
                ok: false,
                mensaje: 'Track no existe',
                errors: err
            });
        }

        //en otro caso
        res.status(200).json({
            ok: true,
            json
        });


    });

}

function insertZona(req, res, next) {
    const shortid = require('shortid');
    var { idAlert, coordinates } = req.body;
   
    let id=shortid.generate()
    let document = new zonaModel({
        idZona:id,
        idAlert: idAlert,
        coordinates: coordinates,
      });
    
      zonaModel.create((err, document) => {
        if (err) {
            return res.status(400).json({
                ok: false,
                mensaje: 'Error al crear el alerta',
                errors: err
            });
        }
        res.status(201).json({
            ok: true,
            mensaje: id
        });
    });

    document.save();
}



function deleteZona(req, res, next) {

    var nameForSearch = req.params.id
    zonaModel.remove({ idZona: nameForSearch })
        .then(zona => {
            if (!zona) {
                return res.status(404).send({
                    message: "zona not found with id " + nameForSearch
                });
            }
            res.send({ message: "zona deleted successfully!" });
        }).catch(err => {
            if (err.kind === 'ObjectId' || err.name === 'NotFound') {
                return res.status(404).send({
                    message: "zona not found with id " + req.params.id
                });
            }
            return res.status(500).send({
                message: "Could not delete zona with id " + req.params.id
            });
        });

}



module.exports = {
    deleteZona,
    getZonasByAlertId,
    insertZona
};
