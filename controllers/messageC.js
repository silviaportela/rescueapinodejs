var messageModel = require('../models/messageM');
var ObjectId = require('mongodb').ObjectID;

//R message
function getMessages(req, res, next) {
    messageModel.find({}).exec((err, json) => {
        if (err) {
            return res.status(500).json({
                ok: false,
                mensaje: 'Error de base de datos',
                errors: err
            });
        }

        res.status(200).json({
            ok: true,
            json
        });
    });
}

//R message by id Alert
function getMessagesByAlertId(req, res, next) {
    var nameForSearch = req.params.id
    messageModel.find({ idAlert: nameForSearch }).sort({ timestamp: 'ascending' }).exec((err, json) => {

        //Si la bbdd no responde
        if (err) {
            return res.status(500).json({
                ok: false,
                mensaje: 'Error al buscar mensajes',
                errors: err
            });
        }

        //si el usuario no existe
        if (!json) {
            return res.status(400).json({
                ok: false,
                mensaje: 'Mensaje no existe',
                errors: err
            });
        }

        //en otro caso
        res.status(200).json({
            ok: true,
            json
        });


    });

}

//C message

function insertMessage(req, res, next) {
    const shortid = require('shortid');
    var { idAlert, timestamp,message,user } = req.body;
    
    let id=shortid.generate()
    let document = new messageModel({
        idMessage:id,
        idAlert: idAlert,
        user:user,
        timestamp: timestamp,
        message:message
      });

    
      messageModel.create((err, document) => {
        if (err) {
            return res.status(400).json({
                ok: false,
                mensaje: 'Error al crear el alerta',
                errors: err
            });
        }
        res.status(201).json({
            ok: true,
            mensaje: id
        });
    });

    document.save();
}


//D message
function deleteOneMessage(req, res, next) {

    var nameForSearch = req.params.idMessage
    messageModel.remove({ idMessage: nameForSearch })
        .then(message => {
            if (!message) {
                return res.status(404).send({
                    message: "Message not found with id " + nameForSearch
                });
            }
            res.send({ message: "message deleted successfully!" });
        }).catch(err => {
            if (err.kind === 'ObjectId' || err.name === 'NotFound') {
                return res.status(404).send({
                    message: "Message not found with id " + req.params.id
                });
            }
            return res.status(500).send({
                message: "Could not delete Message with id " + req.params.id
            });
        });
    }

    //D all messages from and alert
    function deleteAllMessage(req, res, next) {

        var nameForSearch = req.params.idAlert
        messageModel.remove({ idAlert: nameForSearch })
            .then(message => {
                if (!message) {
                    return res.status(404).send({
                        message: "Message not found with id " + nameForSearch
                    });
                }
                res.send({ message: "message deleted successfully!" });
            }).catch(err => {
                if (err.kind === 'ObjectId' || err.name === 'NotFound') {
                    return res.status(404).send({
                        message: "Message not found with id " + req.params.id
                    });
                }
                return res.status(500).send({
                    message: "Could not delete Message with id " + req.params.id
                });
            });
        }
    

module.exports = {
    getMessages,
    getMessagesByAlertId,
    insertMessage,
    deleteOneMessage,
    deleteAllMessage
};
