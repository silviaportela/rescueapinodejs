var trackModel = require('../models/trackM');
var ObjectId = require('mongodb').ObjectID;

//READ tracks
function getTracks(req, res, next) {
    trackModel.find({}).exec((err, json) => {
        if (err) {
            return res.status(500).json({
                ok: false,
                mensaje: 'Error de base de datos',
                errors: err
            });
        }

        res.status(200).json({
            ok: true,
            json
        });
    });
}

//READ tacks by alert
function getTracksByAlertId(req, res, next) {
    var nameForSearch = req.params.id
    trackModel.find({ idAlert: nameForSearch }, (err, json) => {

        //Si la bbdd no responde
        if (err) {
            return res.status(500).json({
                ok: false,
                mensaje: 'Error al buscar tracks',
                errors: err
            });
        }

        //si el usuario no existe
        if (!json) {
            return res.status(400).json({
                ok: false,
                mensaje: 'Track no existe',
                errors: err
            });
        }

        //en otro caso
        res.status(200).json({
            ok: true,
            json
        });


    });

}

//READ tracks by user
function getTracksByUser(req, res, next) {
    var nameForSearch = req.params.user
    trackModel.find({ user: nameForSearch }, (err, json) => {

        //Si la bbdd no responde
        if (err) {
            return res.status(500).json({
                ok: false,
                mensaje: 'Error al buscar tracks',
                errors: err
            });
        }

        //si el usuario no existe
        if (!json) {
            return res.status(400).json({
                ok: false,
                mensaje: 'Track no existe',
                errors: err
            });
        }

        //en otro caso
        res.status(200).json({
            ok: true,
            json
        });


    });

}



//CREATE track
function insertTrack(req, res, next) {
    const shortid = require('shortid');
    var { idAlert, coordinates,user } = req.body;
   
    let id=shortid.generate()
    let document = new trackModel({
        idTrack:id,
        idAlert: idAlert,
        coordinates: coordinates,
        user:user,
      });
    
    trackModel.create((err, document) => {
        if (err) {
            return res.status(400).json({
                ok: false,
                mensaje: 'Error al crear el alerta',
                errors: err
            });
        }
        res.status(201).json({
            ok: true,
            mensaje: id
        });
    });

    document.save();
}


//DELETE track
function deleteTrack(req, res, next) {

    var nameForSearch = req.params.id
    trackModel.remove({ idTrack: nameForSearch })
        .then(track => {
            if (!track) {
                return res.status(404).send({
                    message: "track not found with id " + nameForSearch
                });
            }
            res.send({ message: "track deleted successfully!" });
        }).catch(err => {
            if (err.kind === 'ObjectId' || err.name === 'NotFound') {
                return res.status(404).send({
                    message: "track not found with id " + req.params.id
                });
            }
            return res.status(500).send({
                message: "Could not delete track with id " + req.params.id
            });
        });

}



module.exports = {
    getTracks,
    getTracksByAlertId,
    insertTrack,
    getTracksByUser,
    deleteTrack
};
