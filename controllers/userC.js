var userModel = require('../models/userM');

//Obtener user por nombre
function getUserById(req, res, next) {
	var nameForSearch = req.params.name
	userModel.findOne( {name: nameForSearch}, (err, json)=>{
		
		//Si la bbdd no responde
        if (err){
            return res.status(500).json({
                ok: false,
                mensaje: 'Error al buscar usuarios',
                errors: err
            });
		}

		//si el usuario no existe
		if (!json){
			return res.status(400).json({
                ok: false,
                mensaje: 'Usuario no existe',
                errors: err
            });
		}
		
		//en otro caso
        res.status(200).json({
            ok: true,
            json
        });

        
    });

}

//CREATE track
function createUser(req, res, next) {
    var { name } = req.body;
   
    let document = new userModel({
        name:name,
      });
    
      userModel.create((err, document) => {
        if (err) {
            return res.status(400).json({
                ok: false,
                mensaje: 'Error al crear el user',
                errors: err
            });
        }
        res.status(201).json({
            ok: true,
            mensaje: name
        });
    });

    document.save();
}


module.exports = {
//	getUsers,
    getUserById,
    createUser
};
