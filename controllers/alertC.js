var alertModel = require('../models/alertM');
var ObjectId = require('mongodb').ObjectID;

//Obtener alertas
function getAlerts(req, res, next) {
    alertModel.find({}).exec((err, json) => {
        if (err) {
            return res.status(500).json({
                ok: false,
                mensaje: 'Error de base de datos',
                errors: err
            });
        }

        res.status(200).json({
            ok: true,
            json
        });
    });
}


//Obtener alertas. Se muestran doatos, total de horas acumuladas y número de voluntarios
function getAlertsById(req, res, next) {
    var nameForSearch = req.params.id
    var date = new Date().getTime();
    alertModel.aggregate([
        {
            $match: { _id: ObjectId(nameForSearch) }
        }, {
            $project: {
                count: { $size: "$users" },
                _id: "$_id",
                name: "$name",
                coord1: "$coord1",
                coord2: "$coord2",
                status: "$status",
                city: "$city",
                users: "$users",
                date: "$date",
                puntosImportantes: "$puntosImportantes",
                dateDifference: { $subtract: [date, "$date"] }
            }
        }
    ]).exec((err, json) => {

        res.status(200).json({
            ok: true,
            json
        });
    });
    

}

//Obtener alertas ordenados por fecha
function getAlertsOrdered(req, res, next) {
    alertModel.find({ 'status': 1 }).sort({ date: 'descending' }).exec((err, json) => {
        if (err) {
            return res.status(500).json({
                ok: false,
                mensaje: 'Error de base de datos',
                errors: err
            });
        }

        res.status(200).json({
            ok: true,
            json
        });
    });
}


//C ALERT
function insertAlert(req, res, next) {
    var { name, status, coord1, coord2, city } = req.body;

    let date = Date.now()

    let document = new alertModel({
        name: name,
        status: status,
        coord1: coord1,
        coord2: coord2,
        city: city,
        date: date,
        puntosImportantes: [],
        users: []
    });


    alertModel.create((err, document) => {
        if (err) {
            return res.status(400).json({
                ok: false,
                mensaje: 'Error al crear el alerta',
                errors: err
            });
        }

        res.status(201).json({
            ok: true,
            mensaje: name
        });
    });

    document.save();
}

//D ALERT
function deleteAlert(req, res, next) {

    var nameForSearch = req.params.id
    alertModel.remove({ "_id": nameForSearch })
        .then(alert => {
            if (!alert) {
                return res.status(404).send({
                    message: "alert not found with id " + nameForSearch
                });
            }
            res.send({ message: "alert deleted successfully!" });
        }).catch(err => {
            if (err.kind === 'ObjectId' || err.name === 'NotFound') {
                return res.status(404).send({
                    message: "alert not found with id " + req.params.id
                });
            }
            return res.status(500).send({
                message: "Could not delete alert with id " + req.params.id
            });
        });

}

//U alert
function updateAlert(req, res, next) {

    var nameForSearch = req.params.name
    var status = req.body.status

    alertModel.update({ name: nameForSearch }, { $set: { status: status } }).then((json) => {
        if (json)
            return res.status(200).json({
                ok: true,
                json
            });

        if (!json)
            return res.status(400).json({
                ok: false,
                mensaje: 'Alerta no existe',
                errors: err
            });

    }).catch((err) => {

        res.status(500).json({
            ok: false,
            mensaje: 'Error al buscar usuarios',
            errors: err
        });

    });



}

//Obtener alerta por ID.
function getAlertsByIdAndCount(req, res, next) {
    var nameForSearch = req.params.id
    var date = new Date().getTime();
    alertModel.aggregate([
        {
            $match: { _id: ObjectId(nameForSearch) }
        }, {
            $project: {
                count: { $size: "$users" },
                _id: "$_id",
                name: "$name",
                coord1: "$coord1",
                coord2: "$coord2",
                status: "$status",
                city: "$city",
                users: "$users",
                date: "$date",
                puntosImportantes: "$puntosImportantes",
                dateDifference: { $subtract: [date, "$date"] }
            }
        }
    ]).exec((err, json) => {

        res.status(200).json({
            ok: true,
            json
        });
    });

}

// OBTENER MIS ALERTAS (a las que estoy suscrita)
function getMyAlerts(req, res, next) {
    var nameForSearch = req.params.name
    var date = new Date().getTime();
    alertModel.aggregate([
        {
            $match: { users: nameForSearch }
        }, {
            $project: {
                count: { $size: "$users" },
                _id: "$_id",
                name: "$name",
                coord1: "$coord1",
                coord2: "$coord2",
                status: "$status",
                city: "$city",
                users: "$users",
                date: "$date",
                puntosImportantes: "$puntosImportantes",
                dateDifference: { $subtract: [date, "$date"] }
            }
        }
    ]).exec((err, json) => {
        if (err) {
            return res.status(500).json({
                ok: false,
                mensaje: 'Error de base de datos',
                errors: err
            });
        }
        
        res.status(200).json({
            ok: true,
            json
        });
    });



}

//Obtener las alertas a las que no estoy suscrita
function getNoMyAlerts(req, res, next) {

    var nameForSearch = req.params.name
    var date = new Date().getTime();
    alertModel.aggregate([
        {
            $match: {
                users: { $nin: [nameForSearch] }
            } 
        }, {
            $project: {
                count: { $size: "$users" },
                _id: "$_id",
                name: "$name",
                coord1: "$coord1",
                coord2: "$coord2",
                status: "$status",
                city: "$city",
                users: "$users",
                puntosImportantes: "$puntosImportantes",
                date: "$date",
                dateDifference: { $subtract: [date, "$date"] }
            }
        }
    ]).exec((err, json) => {
        if (err) {
            return res.status(500).json({
                ok: false,
                mensaje: 'Error de base de datos',
                errors: err
            });
        }
        res.status(200).json({
            ok: true,
            json
        });
    });



}

// Apuntarme a alertas
function joinToAlert(req, res, next) {

    var nameForSearch = req.params.name
    var user = req.body.user

    alertModel.update({ name: nameForSearch }, { $push: { users: user } }).then((json) => {
        if (json)
            return res.status(200).json({
                ok: true,
                json
            });

        if (!json)
            return res.status(400).json({
                ok: false,
                mensaje: 'Alerta no existe',
                errors: err
            });

    }).catch((err) => {

        return res.status(500).json({
            ok: false,
            mensaje: 'Error al buscar usuarios',
            errors: err
        });

    });

}


module.exports = {
    getAlerts,
    getAlertsById,
    getAlertsOrdered,
    insertAlert,
    deleteAlert,
    updateAlert,
    getAlertsByIdAndCount,
    getMyAlerts,
    getNoMyAlerts,
    joinToAlert
};
